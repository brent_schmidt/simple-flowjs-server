# Sample code for Node.js

This sample is written for [Node.js](http://nodejs.org/) and requires [Express](http://expressjs.com/) to make the sample code cleaner.

To install and run:

    cd samples/Node.js
    npm install
    node app.js

Then browse to [localhost:4000](http://localhost:4000).

File chunks will be uploaded to samples/Node.js/tmp directory.

## Cross-domain Uploads Enabled

By default, cross-domain uploads are enabled. If you wish to disable them, set the `ACCESS_CONTROL_ALLOW_ORIGIN` variable to `false`.

